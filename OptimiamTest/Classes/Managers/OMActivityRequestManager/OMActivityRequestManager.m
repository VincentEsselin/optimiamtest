//
//  OMActivityRequestManager.m
//  OptimiamTest
//
//  Created by Vincent Esselin on 26/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import "OMActivityRequestManager.h"

#import <AFNetworking/AFNetworking.h>

#import <CoreLocation/CoreLocation.h>

static OMActivityRequestManager *sharedInstance;

@interface OMActivityRequestManager () <CLLocationManagerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic, copy) void (^completionBlock)(NSArray<OMActivityModel *> *);

@property (nonatomic) BOOL requestSent;

@end

@implementation OMActivityRequestManager

+ (instancetype)sharedInstance {
  static dispatch_once_t predicate = 0;
  
  dispatch_once(&predicate, ^
                {
                  sharedInstance = [[self alloc] init];
                });
  
  return sharedInstance;
}

- (void)getLocalActivitiesWithCompletionBlock:(void (^)(NSArray<OMActivityModel *> *))completionBLock
{
    self.completionBlock = completionBLock;
    [self.locationManager requestLocation];
    self.requestSent = NO;
    NSLog(@"fetch location");
}

- (void)launchActivtiesRequestWithLongitude:(CGFloat)longitude andLatitude:(CGFloat)latitude
{
  NSLog(@"Request sent");
  NSString *baseURL = @"https://api.paris.fr/api/data/1.4/QueFaire/get_geo_activities";
  AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
  [sessionManager GET:baseURL parameters:[self parametersForRequestWithLongitude:longitude andLatitude:latitude] progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    self.requestSent = NO;
    NSArray *responseArray = responseObject[@"data"];
    NSMutableArray<OMActivityModel *> *actitivies = [NSMutableArray new];
    for (NSDictionary *activity in responseArray) {
      [actitivies addObject:[OMActivityModel fromJson:activity]];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
      if (self.completionBlock) {
        self.completionBlock([NSArray arrayWithArray:actitivies]);
      }
      self.activities = [NSArray arrayWithArray:actitivies];
      [[NSNotificationCenter defaultCenter] postNotificationName:@"ACTIVITIES_AVAILABLE" object:nil];
    });
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Aucune activité trouvée, vérifiez votre connexion internet" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    });
  }];
}

- (NSDictionary *)parametersForRequestWithLongitude:(CGFloat)longitude andLatitude:(CGFloat)latitude {
  NSMutableDictionary *params = [NSMutableDictionary new];
  
  [params setObject:@"a98ecb467c92f3e1f15b21eef4713545319e46bf80d8173b386de246e11e3394" forKey:@"token"];
  [params setObject:[NSNumber numberWithInteger:0] forKey:@"cid"];
  [params setObject:[NSNumber numberWithInteger:0] forKey:@"tag"];
  [params setObject:[NSNumber numberWithInteger:0] forKey:@"created"];
  [params setObject:[NSNumber numberWithInteger:0] forKey:@"start"];
  [params setObject:[NSNumber numberWithInteger:0] forKey:@"end"];
  [params setObject:[NSNumber numberWithFloat:latitude] forKey:@"lat"];
  [params setObject:[NSNumber numberWithFloat:longitude] forKey:@"lon"];
  [params setObject:[NSNumber numberWithInteger:10000] forKey:@"radius"];
  [params setObject:[NSNumber numberWithInteger:0] forKey:@"offset"];
  [params setObject:[NSNumber numberWithInteger:50] forKey:@"limit"];
  
  return [NSDictionary dictionaryWithDictionary:params];
}

#pragma mark - Smart Getters

- (CLLocationManager *)locationManager {
  if (!_locationManager) {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager requestAlwaysAuthorization];
  }
  return _locationManager;
}


#pragma mark - CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Impossible d'obtenir votre localisation" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.userLatitude && self.userLongitude) {
        [self launchActivtiesRequestWithLongitude:self.userLongitude andLatitude:self.userLatitude];
    } else {
        [self.locationManager requestLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    NSLog(@"location found");
    if (!self.requestSent) {
        if (locations) {
            self.requestSent = YES;
            CLLocation *location = [locations firstObject];
            [self launchActivtiesRequestWithLongitude:location.coordinate.longitude andLatitude:location.coordinate.latitude];
            self.userLongitude = location.coordinate.longitude;
            self.userLatitude = location.coordinate.latitude;
        }
    }
}

@end
