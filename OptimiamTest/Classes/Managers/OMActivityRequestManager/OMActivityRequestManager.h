//
//  OMActivityRequestManager.h
//  OptimiamTest
//
//  Created by Vincent Esselin on 26/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OMActivityModel.h"

@interface OMActivityRequestManager : NSObject

@property (nonatomic) float userLongitude;
@property (nonatomic) float userLatitude;

@property (nonatomic, strong) NSArray<OMActivityModel *> *activities;

+ (instancetype)sharedInstance;

- (void)getLocalActivitiesWithCompletionBlock:(void (^)(NSArray<OMActivityModel *> *activities))completionBLock;

@end
