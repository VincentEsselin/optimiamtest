//
//  OMActivityModel.h
//  OptimiamTest
//
//  Created by Vincent Esselin on 27/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OMActivityModel : NSObject

@property (nonatomic) NSInteger             activityID;
@property (nonatomic, strong) NSString      *name;
@property (nonatomic, strong) NSString      *activityDescription;
@property (nonatomic, strong) NSString      *smallDescription;
@property (nonatomic, strong) NSString      *venue;
@property (nonatomic, strong) NSString      *address;
@property (nonatomic) NSInteger             zipCode;
@property (nonatomic, strong) NSString      *city;
@property (nonatomic) NSString              *latitude;
@property (nonatomic) NSString              *longitude;
@property (nonatomic, strong) NSString      *accessType;
@property (nonatomic) BOOL                  hasFee;
@property (nonatomic, strong) NSString      *createdAt;
@property (nonatomic, strong) NSString      *distance;
@property (nonatomic, strong) NSArray       *categories;
@property (nonatomic, strong) NSString      *imageURL;
@property (nonatomic, strong) NSArray       *occurences;

+ (OMActivityModel *)fromJson:(NSDictionary *)data;

@end
