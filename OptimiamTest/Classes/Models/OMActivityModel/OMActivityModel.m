//
//  OMActivityModel.m
//  OptimiamTest
//
//  Created by Vincent Esselin on 27/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import "OMActivityModel.h"

@implementation OMActivityModel

+ (OMActivityModel *)fromJson:(NSDictionary *)data {
  OMActivityModel *model = [OMActivityModel new];
  
  if (data) {
    NSNumber *tempNumber = data[@"idactivites"];
    model.activityID = tempNumber.integerValue;
    model.name = data[@"nom"];
    model.activityDescription = data[@"description"];
    model.smallDescription = data[@"small_description"];
    model.venue = data[@"lieu"];
    model.address = data[@"adresse"];
    tempNumber = data[@"zipcode"];
    model.zipCode = tempNumber.integerValue;
    model.city = data[@"city"];
    model.latitude = data[@"lat"];
    model.longitude = data[@"lon"];
    model.accessType = data[@"accessType"];
    tempNumber = data[@"hasFee"];
    model.hasFee = (tempNumber.integerValue != 0);
    model.createdAt = data[@"created"];
    model.distance = data[@"distance"];
    
    NSArray *categoriesArray = data[@"rubriques"];
    NSMutableArray *tempArray = [NSMutableArray new];
    for (NSDictionary *category in categoriesArray) {
      [tempArray addObject:category[@"rubrique"]];
    }
    model.categories = [NSArray arrayWithArray:tempArray];
    model.imageURL = [data[@"files"] firstObject][@"path"];
    model.occurences = data[@"occurences"];
//    model.day = [data[@"occurences"] firstObject][@"jour"];
//    model.hourStart = [data[@"occurences"] firstObject][@"hour_start"];
//    model.hourEnd = [data[@"occurences"] firstObject][@"hour_end"];
  }
  
  return model;
}

@end
