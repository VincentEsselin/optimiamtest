//
//  OMActivityCollectionViewCell.m
//  OptimiamTest
//
//  Created by Vincent Esselin on 29/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import "OMActivityCollectionViewCell.h"

#import <AFNetworking/UIImageView+AFNetworking.h>

@interface OMActivityCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation OMActivityCollectionViewCell

- (void)setUpCellWithModel:(OMActivityModel *)model {
    NSURL *imageURL = [NSURL URLWithString:model.imageURL];
    [self.backgroundImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    [self.descriptionLabel setText:model.name];
    [self.addressLabel setText:[NSString stringWithFormat:@"%@, %@, %ld", model.address, model.city, (long) model.zipCode]];
}

- (void)prepareForReuse {
    self.backgroundImageView.image = nil;
}

@end
