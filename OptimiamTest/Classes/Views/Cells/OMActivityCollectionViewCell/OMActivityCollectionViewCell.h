//
//  OMActivityCollectionViewCell.h
//  OptimiamTest
//
//  Created by Vincent Esselin on 29/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OMActivityModel.h"

@interface OMActivityCollectionViewCell : UICollectionViewCell

- (void)setUpCellWithModel:(OMActivityModel *)model;

@end
