//
//  SecondViewController.m
//  OptimiamTest
//
//  Created by Vincent Esselin on 25/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import "OMActivityMapViewController.h"

#import "OMActivityRequestManager.h"

#import "OMActivityModel.h"

#import <MapKit/MapKit.h>

@interface OMActivityMapViewController ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) MKMapView *mapView;

@end

@implementation OMActivityMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self.activityIndicator startAnimating];
    
    if ([OMActivityRequestManager sharedInstance].activities) {
        [self activitiesAvailable:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activitiesAvailable:) name:@"ACTIVITIES_AVAILABLE" object:nil];
    }
}

- (void)activitiesAvailable:(NSNotification *)notification {
    
    [self.activityIndicator stopAnimating];
    
    [self.view addSubview:self.mapView];
    
    [self addActivitesToMap];
}

- (MKMapView *)mapView {
    if (!_mapView) {
        _mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
        _mapView.showsUserLocation = YES;
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake([OMActivityRequestManager sharedInstance].userLatitude, [OMActivityRequestManager sharedInstance].userLongitude);
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, 1000, 1000);
        MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
        [_mapView setRegion:adjustedRegion animated:YES];
    }
    return _mapView;
}

- (void)addActivitesToMap {
    for (OMActivityModel *activity in [OMActivityRequestManager sharedInstance].activities) {
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:CLLocationCoordinate2DMake(activity.latitude.floatValue, activity.longitude.floatValue)];
        [annotation setTitle:activity.name];
        NSString *firstOccurenceDay = [activity.occurences firstObject][@"jour"];
        firstOccurenceDay = [firstOccurenceDay stringByReplacingCharactersInRange:NSMakeRange(10, firstOccurenceDay.length - 10) withString:@""];
        [annotation setSubtitle:firstOccurenceDay];
        [self.mapView addAnnotation:annotation];
    }
}

@end
