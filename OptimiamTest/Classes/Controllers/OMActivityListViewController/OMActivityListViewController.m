//
//  FirstViewController.m
//  OptimiamTest
//
//  Created by Vincent Esselin on 25/04/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import "OMActivityListViewController.h"
#import "OMActivityDetailViewController.h"

#import "OMActivityRequestManager.h"

#import "OMActivityCollectionViewCell.h"

#import "OMActivityModel.h"

@interface OMActivityListViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UICollectionView *activityCollectionView;

@property (strong, nonatomic) NSArray<OMActivityModel *> *activities;

@end

@implementation OMActivityListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self startLoadingData];
  
    NSString *identifier = @"OMActivityCollectionViewCell";
    [self.activityCollectionView  registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellWithReuseIdentifier:identifier];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"collectionToDetail"]) {
        
    }
}


#pragma mark - Requests

- (void)startLoadingData {
    [self.activityIndicator setHidden:NO];
    [self.activityIndicator startAnimating];
    [self.view bringSubviewToFront:self.activityIndicator];
    [[OMActivityRequestManager sharedInstance] getLocalActivitiesWithCompletionBlock:^(NSArray *activities) {
        if (activities) {
            [self.activityIndicator stopAnimating];
            [self.activityIndicator setHidden:YES];
            self.activities = activities;
            [self.activityCollectionView reloadData];
        }
    }];
}


#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.activities.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OMActivityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OMActivityCollectionViewCell" forIndexPath:indexPath];
    
    [cell setUpCellWithModel:self.activities[indexPath.row]];
    
    return cell;
}


#pragma mark - UICollectionViewDelegate


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    OMActivityDetailViewController *detailVC = [[OMActivityDetailViewController alloc] init];
    detailVC.activity = self.activities[indexPath.row];
    [self presentViewController:detailVC animated:YES completion:nil];
}


#pragma mark - UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

@end
