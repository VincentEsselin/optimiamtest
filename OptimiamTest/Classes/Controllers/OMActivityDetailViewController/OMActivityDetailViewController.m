//
//  OMActivityDetailViewController.m
//  OptimiamTest
//
//  Created by Vincent Esselin on 01/05/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import "OMActivityDetailViewController.h"

@interface OMActivityDetailViewController () <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *activityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityTypeLabel;
@property (weak, nonatomic) IBOutlet UITableView *activityHoursTableVIew;

@end

@implementation OMActivityDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.activityNameLabel setText:[NSString stringWithFormat:@"Nom : %@", self.activity.name]];
    [self setActivityType];
}

- (void)setActivityType {
    if (self.activity.categories) {
        NSString *category = [self.activity.categories firstObject];
        if (self.activity.categories.count > 1) {
            for (NSInteger idx = 0; idx < self.activity.categories.count; idx++) {
                category = [NSString stringWithFormat:@"%@, %@", category, self.activity.categories[idx]];
            }
        }
        [self.activityTypeLabel setText:[NSString stringWithFormat:@"Type : %@", category]];
    } else {
        [self.activityTypeLabel setText:@""];
    }
}

#pragma mark - Close Button

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:self completion:nil];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.activity.occurences.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];

    NSDictionary *occurence = self.activity.occurences[indexPath.row];

    NSString *day = occurence[@"jour"];
    day = [day stringByReplacingCharactersInRange:NSMakeRange(10, day.length - 10) withString:@""];
    
    NSString *string = [NSString stringWithFormat:@"%@ %@ %@", day, occurence[@"hour_start"], occurence[@"hour_end"]];
    
    [cell.textLabel setText:string];
    
    return cell;
}

@end
