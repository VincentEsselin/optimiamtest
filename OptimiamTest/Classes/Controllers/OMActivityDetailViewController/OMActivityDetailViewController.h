//
//  OMActivityDetailViewController.h
//  OptimiamTest
//
//  Created by Vincent Esselin on 01/05/2016.
//  Copyright © 2016 Optimiam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OMActivityModel.h"

@interface OMActivityDetailViewController : UIViewController

@property (strong, nonatomic) OMActivityModel *activity;

@end
